<?php
namespace App\Http\Services;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Http;


class ProductService {
    public static function configDatatable() {
        return [
            "heads" => ['Category', 'Product Name', 'Price', 'Stock', 'Thumbnail'],
            "config" => [
                'data' => [],
                'order' => [[1, 'asc']],
                'columns' => [
                    ['data'=>'id', 'orderable'=>false, 'searchable'=>false, 'visible'=>false], 
                    ['data'=>'category'], 
                    ['data'=>'title'],
                    ['data'=>'price', 'orderable' => false],
                    ['data'=>'stock', 'orderable' => false],
                    ['data'=>'thumbnail', 'orderable' => false],
                    // ['data'=>'action', 'orderable' => false, 'searchable'=>false],
                ],
                'serverSide' => true,
                'pageLength' => 50,
                'processing'=>true,
                'searching' => false,
                'ajax' => ['url'=>'/products/get-data']
            ]
    
        ];
    }

    public static function getData($request) {
        $response = Http::get("https://dummyjson.com/products");

        $response = $response->json();
        $products = $response['products'];
        return $products;

        // dd($products);

        // $datatables = Datatables::eloquent($products)
        // ->addColumn('action', function($row) {
            
        //     $buttonGroups = "<div class='btn-group btn-group-justified'>";
        //     $buttonGroups .= "<button type='button' class='btn btn-sm btn-info dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Aksi</button>";
        //     $buttonGroups .= "<div class='dropdown-menu' role='menu'> </div>";

        //     $buttonGroups .= "</div>";
        //     return $buttonGroups;
        // })
        // ->rawColumns(['action'])
        // ->escapeColumns()
        // ->make(false);


        // return $datatables;
    }


}