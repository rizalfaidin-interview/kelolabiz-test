## using docker
- open command prompt
- run <code>./vendor/bin/sail up -d</code> to create and up containers, wait until finish
- run <code>./vendor/bin/sail composer install</code> to install all dependencies
- run <code>./vendor/bin/sail npm run dev</code> to generate style of bootstrap 
- access http://localhost on your browser

## using non docker
- make sure you have installed php, mysql, composer and nodejs on your local machine
- open command prompt
- run <code>composer install</code> to install all dependencies
- run <code>npm run dev</code> to generate style of bootstrap 
- run <code>php run serve</code> to run apps locally using laravel homestead
- access http://localhost:your-port on your browser