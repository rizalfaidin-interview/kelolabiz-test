@extends('adminlte::page')

@section('title', 'Products')

@section('content_header')
    <h1>Products</h1>
    <div class="btn-group">
        <button class="btn btn-success btn-show-create" data-toggle="modal" data-target="#modal-create">create</button>
    </div>

@stop

@section('content')
<table id="product-table" style="width:100%" class="table table-bordered table-hover table-striped table-sm dataTable no-footer" role="grid" aria-describedby="product-table_info">
    <thead class="thead-dark">
        <tr role="row">
            <th>Category</th>
            <th>Product Name + Brand</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Rating</th>
            <th>Thumbnail</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<x-adminlte-modal id="modal-create" title="Create new product" size="lg">
    <form id="product-form">
        <div class="row">
            <div class="col-md-12">
                @csrf
                <input type="hidden" name="id" id="id" value="" />
                <x-adminlte-select name="category" label="Category">
                    <option>fragrances</option>
                    <option>smartphones</option>
                    <option>laptops</option>
                    <option>skincare</option>
                    <option>groceries</option>
                    <option>home-decoration</option>
                </x-adminlte-select>
                <x-adminlte-input name="title" label="Product Name" />
                <x-adminlte-input name="brand" label="Brand" />
                <x-adminlte-textarea name="description" 
                    label="Description" 
                    rows=3
                />
                <x-adminlte-input name="price" label="Price" type="number"/>
                <x-adminlte-input name="stock" label="Existing Stock" type="number"/>
            </div>
        </div>
    </form>

    <x-slot name="footerSlot">
        <x-adminlte-button theme="default" label="Cancel" data-dismiss="modal"/>
        <x-adminlte-button class="ml-auto btn-save" theme="success" label="Save"/>
    </x-slot>
</x-adminlte-modal>

@stop



@section('js')
<script>
var table;
$(document).ready(() => {
    let url = 'https://dummyjson.com/products';
    table = $('#product-table').DataTable( {
        processing: true,
        language: {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        ajax: {
            url: url,
            dataSrc: 'products'
        },
        columns: [
            {data: 'category'},
            {data: 'title', render: (data, type, row)=>{
                return data + ", <b>" + row.brand + "</b> <br> <small>" + row.description + "</small>"
            }}, 
            {data: 'price', render: (data, type, row)=>{
                return "$ "+ data
            }},
            {data: 'stock'},
            {data: 'rating'},
            {data: 'thumbnail', render: (data, type, row)=>{
                return "<img src='"+ data +"' height=70 width=70 />"
            }},
            {data:'id', render: (data, type, row)=>{
                var editBtn = "<button class='btn btn-info btn-sm btn-show-edit' data-id=" + data + " data-toggle='modal' data-target='#modal-create'>Edit</button>";
                var deleteBtn = " <button class='btn btn-danger btn-sm btn-delete' id='btn-delete-"+data+"' data-id=" + data + ">Delete</button>";
                var confirmDeleteBtn = " <button class='btn btn-danger btn-sm btn-confirm-delete' id='btn-confirm-delete-"+data+"' data-id=" + data + " hidden>Confirm Delete</button>";
                return editBtn + deleteBtn + confirmDeleteBtn;
            }}
        ]
    } );
})

$(document).on('click', '.btn-save', (e) => {
    e.preventDefault();

    var product = $('#product-form').serializeArray().reduce((o,kv) => ({...o, [kv.name]: kv.value}), {});
    var isEdit = false;
    var url = 'https://dummyjson.com/products/add';
    var type = 'POST';
    if (!product.title || !product.brand) {
        
        Toast.fire({
            icon: 'error',
            title: 'Wrong input'
        })
        return;
    }
    
    if (product.id) {
        isEdit = true;
        url = 'https://dummyjson.com/products/'+product.id;
        type = 'PUT';
    }

    $.ajax({
        url: url,
        type: type,
        data: product,
        beforeSend: function (xhr) {

        },
        success: function (data) {
            Toast.fire({
                icon: 'success',
                title: 'Product saved successfuly'
            })
            $('#product-form')[0].reset();
            $("#modal-create").modal('toggle');
            table.ajax.reload();
        }
    });

}) 


$(document).on('click', '.btn-show-create', (e)=>{
    e.preventDefault();
    $('#product-form')[0].reset();
    $(".modal-title").text("Create new product");
})

$(document).on('click', '.btn-show-edit', (e)=>{
    e.preventDefault();

    var btnObj = e.target;
    var _id = btnObj.dataset.id;

    $.ajax({
        url: 'https://dummyjson.com/products/'+_id,
        type: 'GET',
        beforeSend: function (xhr) {
        },
        success: function (data) {
            console.log(data);
            $(".modal-title").text("Edit product - " + data.title);
            $('#product-form')[0].reset();
            $("#id").val(data.id);
            $("#category").val(data.category).change();
            $("#title").val(data.title);
            $("#description").val(data.description);
            $("#brand").val(data.brand);
            $("#price").val(data.price);
            $("#stock").val(data.stock);
        }
    });
})


// TODO: Delete function
$(document).on('click', '.btn-delete', (e)=>{
    e.preventDefault();
    $(".btn-delete").attr("hidden", false);
    $(".btn-confirm-delete").attr("hidden", true);

    var btnObj = e.target;
    var _id = btnObj.dataset.id;
    
    $("#btn-delete-"+_id).attr("hidden", true);
    $("#btn-confirm-delete-"+_id).attr("hidden", false);
})

$(document).on('click', '.btn-confirm-delete', (e)=>{
    e.preventDefault();

    var btnObj = e.target;
    var _id = btnObj.dataset.id;

    $.ajax({
        url: 'https://dummyjson.com/products/'+_id,
        type: "DELETE",
        beforeSend: function (xhr) {

        },
        success: function (data) {
            Toast.fire({
                icon: 'success',
                title: 'Product deleted successfuly'
            }) 
            table.ajax.reload();
        }
    });
})


</script>
@stop