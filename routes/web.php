<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\ProductController::class, 'index'])->name('products.index');

Route::get('/products', [App\Http\Controllers\ProductController::class, 'index'])->name('products.index');
Route::get('/products/get-data', [App\Http\Controllers\ProductController::class, 'getData'])->name('products.getData');
Route::get('/products/create', [App\Http\Controllers\ProductController::class, 'create'])->name('products.create');

Auth::routes();

Route::get('/home', [App\Http\Controllers\ProductController::class, 'index'])->name('products.index');
